<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello',function() {
    return 'Hello Laravel';
    
});

Route::get('/student/{id}',function($id = 'No student found') {
    return 'We got Studen with id '.$id;
    
});

Route::get('/car/{id?}',function($id = null) {
    if(isset($id)){
        //TODO: validate for integer
        return "We got car $id"; 
    } else{
        return 'we need the id to find your car';
    }   
    
});

Route::get('/comment/{id}', function ($id) {
    return view('comment',compact('id'));
});

//ex5

Route::get('/users/{email}/{name?}', function ($email,$name = null) {
    //roni add: if (!isset($name)){ $name = 'missing name'}
    return view('users',compact('email','name'));
});

//lesson 6
Route::resource('candidates', 'CandidatesController')->middleware('auth');

Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser')->name('candidate.changeuser');

Route::get('candidates/changestatuse/{cid}/{sid}', 'CandidatesController@changeStatuse')->name('candidate.changestatuse')->middleware('auth');

Route::get('register/changedepartment/{uid}/{did?}', 'RegisterController@changeDepartment')->name('register.changedepartment')->middleware('auth');

Route::get('candidates/delete/{id}', 'CandidatesController@destroy')->name('candidate.delete');

Route::get('mycandidates', 'CandidatesController@showMyCandidates')->name('candidate.mycandidates')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
