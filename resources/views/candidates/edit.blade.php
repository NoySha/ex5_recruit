@extends('layouts.app')
@section('title','Edit candidate')

@section('content')

        <h1 class="text-center font-weight-bold p-3 mb-2 bg-secondary text-white">Edit candidate</h1>
        @if(count($errors) > 0)
           <div class="alert alert-danger">
            <ul>
             @foreach($errors->all() as $error)
                <li>{{$error}}</il>
             @endforeach
             </ul>
        @endif
        <form method ="post" action = "{{action('CandidatesController@update',$candidate->id)}}">
        @method('PATCH')
        @csrf
        <div>
            <input type = "hidden" name = "_method" value = "PATCH"/>
        </div>
        <div>
            <label for = "name" class="pl-5 pt-4" >Candidate name </label>
            <input type = "text" name = "name" value = {{$candidate->name}}>
        </div>
        <div>
            <label for = "email" class="pl-5 pt-3">Candidate email</label>
            <input type = "text" name = "email" value = {{$candidate->email}}>
        </div>
        <div>
            <input type = "submit" name = "submit"  class="btn btn-primary btn-lg font-weight-bold" value="Edit">
        </div>
        </form>
@endsection


