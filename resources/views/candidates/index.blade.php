@extends('layouts.app')
@section('title','Candidate')

@section('content')
                    @if($message = Session::get('success'))
                    <div class = "alert alert-success">
                        <p>{{message}}</p>
                    </div>
                    @endif


                    <h1 class="text-center font-weight-bold p-3 mb-2 bg-secondary text-white"> List of candidates</h1>
                    <table class="table">
                        <tr>
                            <th>id</th><th>Name</th><th>Email</th><th>Owner</th><th>Statuse</th><th>Created</th><th>Updated</th>
                        </tr>
                        <!-- the table data -->
                        @foreach($candidates as $candidate)
                            <tr>
                                <td>{{$candidate->id}}</td>
                                <td>{{$candidate->name}}</td>
                                <td>{{$candidate->email}}</td>
                                <td>
                                     <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        @if(isset($candidate->user_id))
                                            {{$candidate->owner->name}}
                                        @else
                                        Assign owner
                                        @endif
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach($users as $user)

                                        <a class="dropdown-item" href="{{route('candidate.changeuser',[$candidate->id, $user->id])}}">{{$user->name}}</a>

                                    @endforeach
                                    </div>
                                    </div>
                                </td>
                                <td>
                                     <div class="dropdown">
                                     <!-- אם הפונקציה לא חוזרת ריקה, אז יש מעבר בין השלבים ונכניס את זה לדרופ-->
                                     @if (null != App\Statuse::next($candidate->statuse_id))
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    @if(isset($candidate->statuse_id))
                                    {{$candidate->having->name}}
                                        @else
                                        Assign statuse
                                        @endif
                                    </button>
                                    @else
                                    {{$candidate->having->name}}
                                    @endif
                                    @if (App\Statuse::next($candidate->statuse_id) != null)
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach(App\Statuse::next($candidate->statuse_id) as $statuse)
                                        <a class="dropdown-item" href="{{route('candidate.changestatuse',[$candidate->id, $statuse->id])}}">{{$statuse->name}}</a>
                                    @endforeach
                                    @endif
                                    </div>
                                    </div>
                                </td>
                                <td>{{$candidate->created_at}}</td>
                                <td>{{$candidate->updated_at}}</td>
                                <td><a href ="{{action('CandidatesController@edit', $candidate['id'])}}" class="btn btn-primary btn-lg">Edit</a></td>
                                <td><a href ="{{route('candidate.delete',$candidate->id)}}" class="btn btn-primary btn-lg">Delete</a></td>
                            </tr>
                           

                        @endforeach
                    </table>         
                    <div><a href = "{{url('/candidates/create')}}" class="btn btn-primary btn-lg font-weight-bold"> Add new candidate</a></div>          
@endsection

