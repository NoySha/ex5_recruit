@extends('layouts.app')
@section('title','Create candidate')

@section('content')
        <h1 class="text-center font-weight-bold p-3 mb-2 bg-secondary text-white">Create candidate</h1>
        <!-- נותנים הוראה לעביר את הנתונים לפונקציה סטור
        -->
        <form method ="post" action = "{{action('CandidatesController@store')}}">
        @csrf
        <div>
            <label for = "name" class="pl-5 pt-4 ">Candidate name</label>
            <input type = "text"  name = "name">
        </div>
        
        <div>
            <label for = "email" class="pl-5 pt-3 ">Candidate email</label>
            <input type = "text" name = "email">
        </div>
        <div>
            <input type = "submit"  name = "submit"  class="btn btn-primary btn-lg font-weight-bold" value="Create candidate">
        </div>
        </form>
@endsection

