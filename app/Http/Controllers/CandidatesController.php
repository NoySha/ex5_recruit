<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;
use App\User;
use App\Statuse;
use App\Department;
use Illuminate\Support\Facades\Auth;



// full name is "App\Http\Controllers\CandidatesController"
class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        #הפעלת שאילתה בחר כוכבית מקנדידט
        $candidates = Candidate::all();
        $users = User::all();
        $statuses = Statuse::all();

        return view('candidates.index',compact('candidates','users','statuses'));
    }

    public function changeUser($cid, $uid = null){
        $candidate = Candidate::findOrFail($cid);
        $candidate->user_id = $uid;
        $candidate->save();
        return redirect('candidates');
    }


    public function changeStatuse($cid, $sid){
        $candidate = Candidate::findOrFail($cid);
        $from = $candidate->having->statuse_id;
        if (!Statuse::allowed($from,$sid)) return redirect ('candidates');
        /**בחלק הזה אנו שומרים את הנתונים, אם המעבר סטטוס אסור לא נגיע לחלק הזה */
        $candidate->statuse_id = $sid;
        $candidate->save();
        return redirect('candidates');
    }

    public function showMyCandidates(){
        $user = Auth::user();
        $statuses = Statuse::all();
        return view('candidates.mycandidates',compact('user','statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('candidates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $candidate = new Candidate();
        $can = $candidate->create($request-> all());
        $can->statuse_id = 1;
        $can->save();
        return redirect('candidates');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        #מושכת ממסד הנתונים את המשתמש
        $candidate = Candidate::findOrFail($id);
        #שולחת אותם ל view
        return view('candidates.edit',compact('candidate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this-> validate($request, [
            'name' => 'required',
            'email' => 'required'
        ]);
        $candidate = Candidate::findOrFail($id);
        $candidate->update($request-> all());
        return redirect('candidates');

    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // נבדוק שיש מועמד עם המזהה הזה
        $candidate = Candidate::findOrFail($id);
        $candidate->delete();
        return redirect('candidates');

    }
}
