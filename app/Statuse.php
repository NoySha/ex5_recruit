<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Statuse extends Model
{
    protected $fillable = ['name','email'];
    
    public function candidate(){
        return $this->hasMany('App\Candidate');
    }

    public static function next($statuse_id){
        $nextstages = DB::table('nextstages')->where('from',$statuse_id)->pluck('to');
        return self::find($nextstages)->all();

    }
    /**  מניעת שינוי סטטוס בשרת */
    /** היא סטטית כי היא פונקציה כללית  ולא עובדת על אובייקט מסויים*/
    public static function allowed($to,$from){
        /**מריצים שאילתה על הטבלה ואם קיבלנו תוצאה זה אומר שהמעבר מותר- קיימת רשומה כזאת */
        $allowed = DB::table('nextstages')->where('from',$from)->where('to',$to)->get();
        if(isset($allowed)) return true;
        return false;

    }


}
