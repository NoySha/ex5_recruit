<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Candidate extends Model
{
    protected $fillable = ['name','email'];
    
    public function owner(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function having(){
        return $this->belongsTo('App\Statuse', 'statuse_id');
    }

    public static function mycandidate($user_id){
        $candidates = DB::table('candidates')->where('user_id', $user_id)->pluck('id');
        return self::find($candidates)->all();

    }


}
