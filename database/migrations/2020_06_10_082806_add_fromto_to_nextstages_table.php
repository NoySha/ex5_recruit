<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFromtoToNextstagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nextstages', function (Blueprint $table) {
            $table->bigInteger('from')->unsigned()->index;
            $table->bigInteger('to')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nextstages', function (Blueprint $table) {
            //
        });
    }
}
