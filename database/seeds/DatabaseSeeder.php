<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(CandidatesSeeder::class);
        $this->call(StatuseSeeder::class);

        // $this->call(UserSeeder::class);
    }
}
