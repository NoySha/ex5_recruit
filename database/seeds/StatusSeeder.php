<?php

use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            [
                'id' => 1,
                'name' => 'before interview',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 2,
                'name' => 'not fit',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],        
            [
                'id' => 3,
                'name' => 'sent to manager',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 4,
                'name' => 'not fit professionally',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],   
            [
                'id' => 5,
                'name' => 'accepted to work',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],                
            ]);    
    }    }
}
